﻿using FPS.MVC.Services.Authenticate;
using FPS.MVC.Services.Authentication;
using FPS.MVC.Services.Company;

var builder = WebApplication.CreateBuilder(args);

// Cấu hình session
builder.Services.AddSession(options =>
{
    options.IdleTimeout = TimeSpan.FromMinutes(30); // Thiết lập thời gian timeout cho session
    options.Cookie.HttpOnly = true; // Chỉ cho phép cookie được truy cập qua HTTP, không cho phép JavaScript
    options.Cookie.IsEssential = true; // Xác định cookie là cần thiết cho ứng dụng hoạt động
});

builder.Services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();


// Add services to the container.
builder.Services.AddControllersWithViews();

//dang ky service
builder.Services.AddHttpClient<ICompanyService, CompanyService>(c => c.BaseAddress
                                                = new Uri("https://localhost:7268/"));
builder.Services.AddHttpClient<IAuthenticationService, AuthenticationService>(c => c.BaseAddress
                                                = new Uri("https://localhost:7268/"));

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

// Kích hoạt session
app.UseSession();

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

app.UseAuthorization();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");

app.Run();
