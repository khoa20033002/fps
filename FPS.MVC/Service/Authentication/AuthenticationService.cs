﻿using FPS.BusinessLayer.ViewModel.Authentication;
using FPS.BusinessLayer.ViewModel.AuthenticationModel;
using FPS.BusinessLayer.ViewModel.Company;
using FPS.DataLayer.Entity;
using FPS.MVC.ApiConfig;
using FPS.MVC.Service;
using FPS.MVC.Services.Authenticate;
using Newtonsoft.Json;
using NuGet.Common;
using System.IdentityModel.Tokens.Jwt;
using System.Net.Http;
using System.Net.Mail;
using System.Security.Claims;
using System.Text;

namespace FPS.MVC.Services.Authentication
{
    public class AuthenticationService : BaseService, IAuthenticationService
    {
        public AuthenticationService(HttpClient httpClient) : base(httpClient)
        {
        }

        public async Task<SignInResultModel> SignInAsync(SignInModel signInModel)
        {
            var result = new SignInResultModel();

            var jsonContent = JsonConvert.SerializeObject(signInModel);
            var httpContent = new StringContent(jsonContent, Encoding.UTF8, "application/json");

            var response = await _client.PostAsync(PathConfig.SIGN_IN, httpContent);
            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                var tokenHandler = new JwtSecurityTokenHandler();
                var jwtToken = tokenHandler.ReadJwtToken(content);

                var model = new SignInModel
                {
                    Email = jwtToken.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value
                };

                result.Role = jwtToken.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Role)?.Value;
                result.AccessToken = content;
                result.SignInModel = model;
            }

            return result;
        }

        public async Task<SignUpResultModel> SignUpAsync(SignUpModel signUpModel)
        {
            var result = new SignUpResultModel();

            var jsonContent = JsonConvert.SerializeObject(signUpModel);
            var httpContent = new StringContent(jsonContent, Encoding.UTF8, "application/json");

            var response = await _client.PostAsync(PathConfig.SIGN_UP, httpContent);
            if (response.IsSuccessStatusCode)
            {
                //await SendEmailAsync(signUpModel.Email);
                return result;
            }
            return null;
        }

        //public async Task SendEmailAsync(string recipientEmail)
        //{
        //    System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage();
        //    mail.To.Add("to gmail address");
        //    mail.From = new MailAddress("from gmail address", "Email head", System.Text.Encoding.UTF8);
        //    mail.Subject = "This mail is send from asp.net application";
        //    mail.SubjectEncoding = System.Text.Encoding.UTF8;
        //    mail.Body = "This is Email Body Text";
        //    mail.BodyEncoding = System.Text.Encoding.UTF8;
        //    mail.IsBodyHtml = true;
        //    mail.Priority = MailPriority.High;
        //    SmtpClient client = new SmtpClient();
        //    client.Credentials = new System.Net.NetworkCredential("from gmail address", "your gmail account password");
        //    client.Port = 587;
        //    client.Host = "smtp.gmail.com";
        //    client.EnableSsl = true;
        //    try
        //    {
        //        client.Send(mail);
        //    }
        //    catch (Exception ex)
        //    {
        //        Exception ex2 = ex;
        //        string errorMessage = string.Empty;
        //        while (ex2 != null)
        //        {
        //            errorMessage += ex2.ToString();
        //            ex2 = ex2.InnerException;
        //        }
        //    }
        //}
    }
}
