﻿using FPS.BusinessLayer.ViewModel.Authentication;
using FPS.BusinessLayer.ViewModel.AuthenticationModel;

namespace FPS.MVC.Services.Authenticate
{
    public interface IAuthenticationService
    {
        public Task<SignInResultModel> SignInAsync(SignInModel signInModel);

        public Task<SignUpResultModel> SignUpAsync(SignUpModel signUpModel);
    }
}
