﻿using FPS.BusinessLayer.ViewModel.Company;

namespace FPS.MVC.Services.Company
{
    public interface ICompanyService
    {
        public Task<List<CompanyModel>> GetAllCompany(string token);
    }
}
