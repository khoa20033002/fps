﻿using FPS.BusinessLayer.ViewModel.Company;
using FPS.MVC.ApiConfig;
using FPS.MVC.Service;
using Newtonsoft.Json;
using NuGet.Common;
using System.Net.Http;
using System.Net.Http.Headers;

namespace FPS.MVC.Services.Company
{
    public class CompanyService : BaseService, ICompanyService
    {
        public CompanyService(HttpClient httpClient) : base(httpClient)
        {

        }

        public async Task<List<CompanyModel>> GetAllCompany(string token)
        {
            var request = new HttpRequestMessage(HttpMethod.Get, PathConfig.GET_ALL_COMPANY);
            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", token);
            var response = await _client.SendAsync(request);

            if (response.IsSuccessStatusCode)
            {
                // Xử lý kết quả thành công
                var content = await response.Content.ReadAsStringAsync();
                var companies = JsonConvert.DeserializeObject<List<CompanyModel>>(content);
                return companies;
            }
            else
            {
                // Xử lý kết quả không thành công
                // Ví dụ: ném một exception hoặc trả về null
                throw new Exception($"Yêu cầu không thành công, mã lỗi: {response.StatusCode}");
            }
        }

    }
}
