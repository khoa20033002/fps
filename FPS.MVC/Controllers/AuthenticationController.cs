﻿using FPS.BusinessLayer.ViewModel.Authentication;
using FPS.BusinessLayer.ViewModel.AuthenticationModel;
using FPS.MVC.Models;
using FPS.MVC.Services.Authenticate;
using FPS.MVC.Services.Company;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using NuGet.Protocol;
using System.Diagnostics;

namespace FPS.MVC.Controllers
{
    public class AuthenticationController : Controller
    {
        private readonly IAuthenticationService _authenService;

        private readonly IHttpContextAccessor _httpContextAccessor;

        public AuthenticationController(IAuthenticationService authenService, IHttpContextAccessor httpContextAccessor)
        {
            _authenService = authenService;
            _httpContextAccessor = httpContextAccessor;
        }

        public async Task<IActionResult> Index(SignInModel model)
        {
            if (ModelState.IsValid)
            {
                var result = await _authenService.SignInAsync(model);
                var token = result.AccessToken;

                if (token != null)
                {
                    if (!string.IsNullOrEmpty(token.ToString()))
                    {
                        HttpContext.Session.SetString("AccessToken", token.ToString());
                        return RedirectToAction("Index", "Home");
                    }
                }
            }

            // ModelState không hợp lệ hoặc đăng nhập không thành công, hiển thị lại form với thông báo lỗi
            ModelState.AddModelError("", "Login failed. Please try again.");
            return View(model);
        }

        public async Task<IActionResult> SignUp(SignUpModel model)
        {
            if (ModelState.IsValid)
            {
                var result = await _authenService.SignUpAsync(model);
                if (result != null)
                {
                    return RedirectToAction("Index", "Authentication");
                }
            }

            // ModelState không hợp lệ hoặc đăng nhập không thành công, hiển thị lại form với thông báo lỗi
            ModelState.AddModelError("", "Signup Failed. Please try again.");
            return View(model);
        }

        public IActionResult Logout()
        {
            _httpContextAccessor.HttpContext.Session.Clear();
            return RedirectToAction("Index", "Home");
        }


        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
