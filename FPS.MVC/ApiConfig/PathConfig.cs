﻿namespace FPS.MVC.ApiConfig
{
    public class PathConfig
    {
        public const string GET_ALL_COMPANY = "api/Company/GetAll";

        public const string SIGN_IN = "/api/Authenticate/SignIn";

        public const string SIGN_UP = "/api/Authenticate/SignUp";
    }
}
