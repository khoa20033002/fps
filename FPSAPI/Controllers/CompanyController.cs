﻿using FPS.BusinessLayer.IRepository;
using FPS.BusinessLayer.ViewModel.Company;
using FPS.DataLayer.Entity;
using FPS.DataLayer.Helpers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FPS.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CompanyController : ControllerBase
    {
        public readonly ICompanyRepository _companyRepository;

        public CompanyController(ICompanyRepository companyRepository)
        {
            _companyRepository = companyRepository;
        }

        [HttpPost]
        [Authorize(Roles = AppRole.Admin)]
        [Route("AddCompanny")]
        public void AddCompany(CompanyModel model)
        {
            _companyRepository.AddCompany(model);
        }

        [HttpPut]
        [Authorize(Roles = AppRole.Admin)]
        [Route("UpdateCompany")]
        public void UpdateCompany(CompanyModel model)
        {
            _companyRepository.UpdateCompany(model);
        }

        [HttpGet]
        [Route("GetByID")]
        [Authorize(Roles = AppRole.Admin)]
        [ProducesResponseType(typeof(CompanyModel), 200)]
        public IActionResult GetByID(Guid companyID)
        {
            var company = _companyRepository.GetByID(companyID);
            if (company == null)
            {
                return NotFound(); 
            }
            return Ok(company);
        }


        [HttpGet]
        [Authorize]
        [Route("GetAll")]
        [ProducesResponseType(typeof(CompanyModel), 200)]
        public IActionResult GetAll()
        {
            var company = _companyRepository.GetAll();
            if (company == null)
            {
                return NotFound();
            }
            return Ok(company);
        }


        [HttpPut]
        [Authorize(Roles = AppRole.Admin)]
        [Route("DeleteCompany")]
        public void DeleteCompany(Guid companyId)
        {
            _companyRepository.DeleteCompany(companyId);
        }

        [HttpPut]
        [Authorize(Roles = AppRole.Admin)]
        [Route("UnDeleteCompany")]
        public void UnDeleteCompany(Guid companyId)
        {
            _companyRepository.UnDeleteCompany(companyId);
        }
    }
}
