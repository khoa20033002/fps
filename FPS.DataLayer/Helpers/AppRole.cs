﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPS.DataLayer.Helpers
{
    public static class AppRole
    {
        public const string Admin = "Administrator";
        public const string Staff = "Staff";
        public const string Accountant = "Accountant";
    }
}
