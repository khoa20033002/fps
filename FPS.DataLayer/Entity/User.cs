﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPS.DataLayer.Entity
{
    public class User : IdentityUser
    {
        //public string Email {  get; set; }
        //public string Password { get; set; }
        //public string Full_Name {  get; set; }
        //public string Nickname {  get; set; }
        //public DateTime DoB {  get; set; }
        //public string PhoneNumber {  get; set; }
        //public string Address { get; set; }
        //public string Gender { get; set; }
        //public string Image {  get; set; }
        //public int Dep_id {  get; set; }
        //public int Role_id {  get; set; }
        //public string Position {  get; set; }
        //public string Status { get; set; }
        public string FullName { get; set; } = null!;
        public DateTime DOB { get; set; }
        public string? Address { get; set; }
        public bool Status { get; set; }
    }
}
