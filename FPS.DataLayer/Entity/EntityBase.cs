﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPS.DataLayer.Entity
{
    public class EntityBase
    {
        public Guid Id { get; set; }
        public Guid CreatedBy { get; set; }
        public Guid UpdatedBy { get; set;}
        public DateTime? CreatedDate {  get; set; }
        public DateTime? UpdatedDate { get;set; }
        public bool IsDeleted { get; set; }

    }
}
