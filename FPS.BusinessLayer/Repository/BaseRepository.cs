﻿using FPS.DataLayer.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPS.BusinessLayer.Repository
{
    public class BaseRepository
    {
        public readonly FPSContext _context;

        public BaseRepository(FPSContext context)
        {
            _context = context;
        } 
    }
}
