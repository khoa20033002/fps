﻿using FPS.BusinessLayer.ViewModel.Authentication;
using FPS.BusinessLayer.ViewModel.AuthenticationModel;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPS.BusinessLayer.Repository.Account
{
    public interface IAccountRepository
    {
        public Task<IdentityResult> SignUpAsync(SignUpModel model);

        public Task<string> SignInAsync(SignInModel model);
    }
}
