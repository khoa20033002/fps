﻿using FPS.BusinessLayer.IRepository;
using FPS.BusinessLayer.ViewModel.Company;
using FPS.DataLayer.Context;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPS.BusinessLayer.Repository.Company
{
    public class CompanyRepository : BaseRepository, ICompanyRepository
    {
        public CompanyRepository(FPSContext context) : base(context)
        {

        }

        public void AddCompany(CompanyModel model)
        {
            FPS.DataLayer.Entity.Company company = new FPS.DataLayer.Entity.Company();
            company.Id = Guid.NewGuid();
            company.CompanyName = model.CompanyName;
            company.UpdatedDate = DateTime.Now;
            company.CreatedDate = DateTime.Now;
            company.UpdatedBy = (Guid)model.UpdatedBy;
            company.CreatedBy = (Guid)model.CreatedBy;
            _context.Companies.Add(company);
            _context.SaveChanges();
        }

        public void UpdateCompany(CompanyModel model)
        {
            var existingCompany = _context.Companies.FirstOrDefault(c => c.Id == model.Id);
            if (existingCompany != null)
            {
                existingCompany.CompanyName = model.CompanyName;
                existingCompany.UpdatedDate = DateTime.Now;
                existingCompany.UpdatedBy = (Guid)model.UpdatedBy;
                _context.SaveChanges();
            }
            else
            {
                Console.WriteLine("Company with ID " + model.Id + " not found.");
            }
        }

        public List<CompanyViewModel> GetAll()
        {
            var companies = _context.Companies.Where(c => !c.IsDeleted).ToList();

            var companyModels = companies.Select(company => new CompanyViewModel
            {
                Id = company.Id,
                CompanyName = company.CompanyName
            }).ToList();

            return companyModels;
        }
        public CompanyViewModel GetByID(Guid companyId)
        {
            var company = _context.Companies.FirstOrDefault(c => c.Id == companyId && !c.IsDeleted);

            if (company != null)
            {
                var companyModel = new CompanyViewModel
                {
                    Id = company.Id,
                    CompanyName = company.CompanyName
                };

                return companyModel;
            }
            else
            {
                Console.WriteLine("Company with ID " + companyId + " not found.");
                return null;
            }
        }
        public void DeleteCompany(Guid companyId)
        {
            Guid UserId = Guid.NewGuid();
            var companyToDelete = _context.Companies.FirstOrDefault(c => c.Id == companyId);

            if (companyToDelete != null)
            {
                companyToDelete.IsDeleted = true;
                companyToDelete.UpdatedDate = DateTime.Now;
                companyToDelete.UpdatedBy = UserId;
                _context.SaveChanges();
            }
            else
            {
                Console.WriteLine("Company with ID " + companyId + " not found.");
            }
        }

        public void UnDeleteCompany(Guid companyId)
        {
            Guid UserId = Guid.NewGuid();
            var companyToDelete = _context.Companies.FirstOrDefault(c => c.Id == companyId);

            if (companyToDelete != null)
            {
                companyToDelete.IsDeleted = false;
                companyToDelete.UpdatedDate = DateTime.Now;
                companyToDelete.UpdatedBy = UserId;
                _context.SaveChanges();
            }
            else
            {
                Console.WriteLine("Company with ID " + companyId + " not found.");
            }
        }
    }
}
