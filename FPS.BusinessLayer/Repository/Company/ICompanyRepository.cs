﻿using FPS.BusinessLayer.ViewModel.Company;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPS.BusinessLayer.IRepository
{
    public interface ICompanyRepository
    {
        public void AddCompany(CompanyModel model);
        public void UpdateCompany(CompanyModel model);
        public List<CompanyViewModel> GetAll();
        public CompanyViewModel GetByID(Guid companyId);
        public void DeleteCompany(Guid companyId);
        public void UnDeleteCompany(Guid companyId);
    }
}
