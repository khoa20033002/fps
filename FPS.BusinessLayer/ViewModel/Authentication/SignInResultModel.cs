﻿using FPS.BusinessLayer.ViewModel.AuthenticationModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPS.BusinessLayer.ViewModel.Authentication
{
    public class SignInResultModel
    {
        public string AccessToken { get; set; }
        public SignInModel SignInModel { get; set; }
        public string Role { get; set; }
    }
}
